<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('login', function () {
    return view('auth/login');
});

//Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
//Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

Route::group(['middleware' => ['auth']], function() {

	Route::get('lang/{lang}', function($lang) {
	  \Session::put('lang', $lang);
	  return \Redirect::back();
	})->middleware('web')->name('change_lang');
		
    Route::get('/', 'productController@frontProducts');
	
	Route::get('products', 'productController@frontProducts');
	Route::get('products/{id}', 'productController@frontProduct');

	Route::resource('admin/comments', 'commentController');	
});

Route::group(['middleware' => ['CheckAdminMiddleware']], function()
{
   	Route::get('admin', 'HomeController@index')->name('home');

	Route::resource('admin/products', 'productController');
	Route::post('admin/products/{id}/delete', 'productController@forcedelete');

});