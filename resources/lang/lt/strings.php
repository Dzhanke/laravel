<?php

return [

    'products' => 'Produktai',
	'comments' => 'Komentarai',
	'nocomments' => 'Nėra komentarų',
	'submit' => 'Siųsti',
	'tocomment' => 'Komentuoti',
];