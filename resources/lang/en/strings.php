<?php

return [

    'products' => 'Products',
    'noproducts' => 'Where are no products',
	'comments' => 'Comments',
	'nocomments' => 'Where are no comments',
	'submit' => 'Submit',
	'tocomment' => 'Comment',
];