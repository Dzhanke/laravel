<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')
</head>
<body>
    <div class="container">
        <div class="row">
            <div id="sidebar" class="col-xs-3">
				<nav>
					@include('includes.nav')
				</nav>
            </div>
            <div id="main" class="col-xs-9">
                <section>
						<a href="{{ route('change_lang', ['lang' => 'lt']) }}">LT</a>
						<a href="{{ route('change_lang', ['lang' => 'en']) }}">EN</a>
                        @yield('content')

                </section>

            </div>
        </div>
    </div>
</body>
</html>