@extends('layouts.left')
@section('content')
    <h1>{{ $product['code'] }}</h1>
    <div class="description">
        {!! $product['description'] !!}
    </div>

	<div class="comments">
		<h2>@lang('strings.comments')</h2>
		@include('comment.list')
		
		@include('comment.form', array('product_id' => $product['id']))
	</div>
@stop
