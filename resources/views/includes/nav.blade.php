@if($navigation->count())
<h2>@lang('strings.products')</h2>
<ul>
	@foreach($navigation as $item)
		<li>
			<a href="{{ url('/') }}/products/{{ $item->id }}">
				{{ $item->code }}
			</a>
		</li>
	@endforeach
</ul>
@endif