@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{ Form::open(array('action' => 'commentController@store')) }}
	{{ Form::hidden('product_id', $product_id) }}
	{{ Form::hidden('user_id', Auth::user()->id) }}
	{{ Form::hidden('hidden', '0') }}
    <div class="form-group">
		{{ Form::label('comment', __('strings.tocomment')) }}
		{{ Form::textarea('comment',null,array('class' => 'form-control')) }}
    </div>
    {{ Form::submit(__('strings.submit'), array('class' => 'btn btn-primary')) }}

{{ Form::close() }}