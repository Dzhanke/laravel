@if (count($comments))
	<dl class="dl-horizontal">
		@foreach ($comments as $comment)
			<dt><span class="glyphicon glyphicon-user"></span> {{ $comment['user'] }}</dt>
			<dd>{{ $comment['comment'] }}</dd>
		@endforeach
	</dl>
@else
	<p class="alert">@lang('strings.nocomments')</p>
@endif