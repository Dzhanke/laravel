@extends('adminlte::page')

@section('content_header')
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
    <h1>Comments</h1>
@stop

@section('content')
	<div class="box box-info">
		@if ($comments->count())
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>Comment</th>
						<th>Hide</th>
					</tr>
					@foreach($comments as $comment)
					<tr>
						<td>
							{{ $comment->comment }}							
						</td>					
						<td>
							{{ Form::open(array('action' => array('commentController@update', $comment->id), 'method' => 'put')) }}
							@if ($comment->hidden == 1)
								{{ Form::hidden('hidden', '0') }}
								{{ Form::submit('show', array('class' => 'btn btn-success')) }}
							@else
								{{ Form::hidden('hidden', '1') }}
								{{ Form::submit('hide', array('class' => 'btn btn-danger')) }}
							@endif
							
							{{ Form::close() }}
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		@else
			<p class="alert">Where are no comments</p>
		@endif
	</div>
@stop