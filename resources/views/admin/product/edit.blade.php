@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit product</h1>
@stop

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{ Form::open(array('action' => array('productController@update', $product->id), 'method' => 'put')) }}
	
    <div class="form-group">
        {{ Form::label('code', 'Code') }}
        {{ Form::text('code', $product->code, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
		{{ Form::label('description_en', 'Description en') }}
		{{ Form::textarea('description_en',$product->description_en,array('class' => 'form-control multi-content', 'id' => 'content-en')) }}
    </div>
    <div class="form-group">
		{{ Form::label('description_lt', 'Description lt') }}
   		{{ Form::textarea('description_lt',$product->description_lt,array('class' => 'form-control multi-content', 'id' => 'content-en')) }}
    </div>

    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop
@section('adminlte_js')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>

<!-- Initialize the editor. -->
<script>
  $(function() {
    $('textarea').froalaEditor()
  });
</script>
@stop
@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
@stop