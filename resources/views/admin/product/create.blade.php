@extends('adminlte::page')

@section('content_header')
    <h1>Add new product</h1>
@stop

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{ Form::open(array('url' => 'admin/products')) }}
    <div class="form-group">
        {{ Form::label('code', 'Code') }}
        {{ Form::text('code', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
		{{ Form::label('description_en', 'Description en') }}
		{{ Form::textarea('description_en',null,array('class' => 'form-control multi-content', 'id' => 'content-en')) }}
    </div>
    <div class="form-group">
		{{ Form::label('description_lt', 'Description lt') }}
   		{{ Form::textarea('description_lt',null,array('class' => 'form-control multi-content', 'id' => 'content-en')) }}
    </div>

    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop
@section('adminlte_js')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>

<script>
  $(function() {
    $('textarea').froalaEditor()
  });
</script>
@stop
@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
@stop