@extends('adminlte::page')

@section('content_header')
    <h1>{{ $product->code }}</h1>
@stop

@section('content')
	<div class="description">
        {{ $product->description_en }}
    </div>
	<div class="description">
        {{ $product->description_lt }}
    </div>
@stop