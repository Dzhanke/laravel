@extends('adminlte::page')

@section('content_header')
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
	<p><a href="{{ URL::to('admin/products/create') }}">Add new</a></p>
    <h1>Products</h1>
@stop

@section('content')
	<div class="box box-info">
		@if ($products->count())
			<table class="table table-striped">
				<tbody>
					<tr>
						<th>Product</th>
						<th>Edit</th>
						<th>Delete</th>
						<th>Force Delete</th>
					</tr>
					@foreach($products as $product)
					<tr>
						<td>
							<a href="{{ url('admin/products/' . $product->id) }}">{{ $product->code }}</a>
							
						</td>
						<td>
							<a href="{{ url('admin/products/' . $product->id). '/edit' }}" class="btn btn-success">edit</a>
						</td>
						<td>
							{{ Form::open(array('url' => 'admin/products/' . $product->id, 'class' => 'pull-left')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('delete', array('class' => 'btn btn-warning')) }}
							{{ Form::close() }}
						</td>
						<td>
							{{ Form::open(array('url' => 'admin/products/' . $product->id . '/delete', 'class' => 'pull-left')) }}
								
								{{ Form::submit('delete', array('class' => 'btn btn-danger')) }}
							{{ Form::close() }}
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		@else
			<p class="alert">Where are no products</p>
		@endif
	</div>
@stop