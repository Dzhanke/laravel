<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Product;
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;

class productController extends Controller
{
 	public function index() {
		$products = Product::orderBy('id','desc')->get();
		return view('admin.product.index', array('products' => $products));
	}
	
    public function show($id) {
		$product = Product::find($id);
		$locale = App::getLocale();
		return view('admin.product.show', array('product' => $product));
	}
	
	public function edit($id) {
		$product = Product::find($id);
    	return view('admin.product.edit', array('product' => $product));
    }
	
	public function update($id) {
		
		$rules = array(
			'code'       => 'required',
			'description_en'      => 'required',
			'description_lt' => 'required'
		);
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('admin/products/' . $id . '/edit')->withErrors($validator);
        } else {
            
            $product = Product::find($id);
            $product->code = Input::get('code');
			$product->description_en = Input::get('description_en');
			$product->description_lt = Input::get('description_lt');
			$product->save();

    		Session::flash('message', 'Updated');
			return redirect('admin/products');
		}
	}
	
	public function create() {
        return view('admin.product.create');
    }
	
	public function store() {
		$rules = array(
			'code'       => 'required',
			'description_en'      => 'required',
			'description_lt' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return redirect('admin/products/create')
				->withErrors($validator);
		} else {

			$product = new Product;
			$product->code = Input::get('code');
			$product->description_en = Input::get('description_en');
			$product->description_lt = Input::get('description_lt');
			$product->save();

			Session::flash('message', 'Product created');
			return redirect('admin/products');
		}
    }

	public function destroy($id) {
	  	$product = Product::find($id);
		$product->delete();
	
		Session::flash('message', $product->code . ' deleted');
		return redirect('admin/products');
	}
	
	public function forcedelete($id) {
		$product = Product::find($id);
		$product->forceDelete();
		
		Session::flash('message', $product->code . ' deleted for forever');
		return redirect('admin/products');
	}
	
	public function frontProducts() {
		
		return view('product.list');
	}
	
    public function frontProduct($id) {
		$product = Product::find($id);
		$locale = App::getLocale();
		
		$tmpProduct = [
			'id' => $product->id,
			'code' => $product->code,
			'description' => $product->{'description_'.$locale}
		];
		
		$tmpComments = [];
		$comments = Comment::where('product_id', $product->id)->where('hidden', 0)->orderBy('id','desc')->get();
		
		foreach($comments->toArray() as $comment) {
			$single = ['user' => User::find($comment['user_id'])->name,
	  		'comment' => $comment['comment']];
			array_push($tmpComments,$single);
		}
				
		return view('product.show', array('product' => $tmpProduct, 'comments' => $tmpComments));
	
	}
}