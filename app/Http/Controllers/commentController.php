<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;

class commentController extends Controller
{
	public function showCommentsInProduct($id) {
		$comment = Comment::orderBy('id','desc')->get();
		return view('comment.list', array('comments' => $comment));
	}
	public function index() {
		$comments = Comment::orderBy('id','desc')->get();
		return view('admin.comment.index', array('comments' => $comments));
	}
	
	public function update($id) {
		
		$product = Comment::find($id);
		$product->hidden = Input::get('hidden');
		$product->save();

		Session::flash('message', 'Updated');
		return redirect('admin/comments');
		
	}
	public function store() {
		$rules = array(
			'comment' => 'required'
		);
		
		$validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('products/' . Input::get('product_id'))->withErrors($validator);
        } else {
            
		$product = new Comment;
		$product->product_id = Input::get('product_id');
		$product->user_id = Input::get('user_id');
		$product->comment = Input::get('comment');
		$product->hidden = Input::get('hidden');
		$product->save();

		return redirect('products/' . Input::get('product_id'));
		}
    }
}
