<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Session::has("lang")) {
            $lang = Session::get("lang");
        	App::setLocale($lang);
        } else {
			App::setLocale('en');
		}
        return $next($request);
    }
}
